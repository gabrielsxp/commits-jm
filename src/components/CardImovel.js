import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'CardImovel',
  mixins: [Utilidades],
  props: {
    dados: {
      type: Object
    }
  }
}
