export default {
  name: 'Filtros',
  data () {
    return {
      filtro: 'Comprar',
      categorias: [
        { label: 'Apartamento', marcado: 0 },
        { label: 'Casa Comercial ', marcado: 0 },
        { label: 'Casa de Vila', marcado: 0 },
        { label: 'Casa em Condomínio', marcado: 0 },
        { label: 'Casas', marcado: 0 },
        { label: 'Coberturas', marcado: 0 }
      ]
    }
  }
}
