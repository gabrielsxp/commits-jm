import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'Header',
  mixins: [Utilidades],
  data () {
    return {
      rotaAtual: ''
    }
  },
  mounted () {
    const rota = this.$router.currentRoute.name
    this.capitalizar(rota)
  },
  methods: {
    capitalizar (rota) {
      const nome = rota.split('_')
      if (nome.length > 0) {
        for (let i = 0, x = nome.length; i < x; i++) {
          nome[i] = nome[i][0].toUpperCase() + nome[i].substr(1)
        }
        this.rotaAtual = nome.join(' ')
      } else {
        this.rotaAtual = rota
      }
    }
  }
}
