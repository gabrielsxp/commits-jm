import CardImovel from 'src/components/CardImovel.vue'
import CardProcura from 'src/components/CardProcura.vue'
import Utilidades from 'src/mixins/Utilidades'
import CarrosselCards from 'src/components/CarrosselCards.vue'

export default {
  name: 'PageIndex',
  mixins: [Utilidades],
  components: { CardImovel, CardProcura, CarrosselCards },
  data () {
    return {
      busca: ''
    }
  },
  methods: {
    descer () {
      const el = this.$refs.destaques
      el.scrollIntoView({ behavior: 'smooth' })
    },
    buscar () {
      this.irRota('busca')
    }
  }
}
