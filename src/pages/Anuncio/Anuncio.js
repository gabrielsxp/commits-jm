import Header from 'src/components/Header.vue'
import Carrossel from 'src/components/CarrosselAnuncio.vue'
import CarrosselCards from 'src/components/CarrosselCards.vue'
import CardImovel from 'src/components/CardImovel.vue'

export default {
  name: 'Anuncio',
  components: { Header, Carrossel, CardImovel, CarrosselCards },
  data () {
    return {
      whatsapp: false,
      settings: {
        dots: false,
        arrows: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        swipe: true,
        centerMode: false,
        slidesToScroll: 4,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1500,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 1100,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 720,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              swipe: true
            }
          },
          {
            breakpoint: 470,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              swipe: true
            }
          }
        ]
      },
      caracteristicas: [
        'Área de Serviço',
        'Lavabo',
        'Copa e Cozinha',
        'Móveis Planejados',
        'Despensa',
        'Reformado',
        'Escritório',
        'Sala de Jantar',
        'Hidromassagem',
        'Suíte Master'
      ],
      condominio: [
        'Jardim',
        'Portaria'
      ],
      imgs: [
        'statics/anuncio/1.jpg',
        'statics/anuncio/2.jpg',
        'statics/anuncio/3.jpg',
        'statics/anuncio/4.jpg',
        'statics/anuncio/5.jpg'
      ]
    }
  }
}
